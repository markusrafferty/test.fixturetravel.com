Fixture Travel Test
===================

### Getting started

 * Install VirtualBox version 6.0.10+ (https://www.virtualbox.org);
 * Install Vagrant version 2.2.5+ (http://www.vagrantup.com/).

### Setting it up

 1. Clone this repository;
 2. Open a command prompt;
 3. Run `vagrant up` in your local repository's directory;
 4. Run `cd fixturetravel/ && composer install` to download dependencies;
 5. Add `192.168.102.101   test.fixturetravel.com` to your hosts file;
 6. Visit *test.fixturetravel.com* in your browser and see if it works.
